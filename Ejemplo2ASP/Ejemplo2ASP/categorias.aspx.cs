﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo2ASP
{
    public partial class categorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NorthwindEntities db = new NorthwindEntities();
                var lista = (from f in db.Categories
                             select new
                             {
                                 f.CategoryID,
                                 f.CategoryName
                             }).ToList();

                lstCategorias.DataSource = lista;
                lstCategorias.DataTextField = "CategoryName";
                lstCategorias.DataValueField = "CategoryID";
                lstCategorias.DataBind();    
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            NorthwindEntities db = new NorthwindEntities();
            var lista = (from f in db.Categories
                         where f.CategoryName.Contains(txtFiltro.Text)
                         select new
                         {
                             f.CategoryID,
                             f.CategoryName
                         }).ToList();

            lstCategorias.DataSource = lista;
            lstCategorias.DataTextField = "CategoryName";
            lstCategorias.DataValueField = "CategoryID";
            lstCategorias.DataBind();

        }

        protected void lstCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db = new NorthwindEntities();
            var id = int.Parse( lstCategorias.SelectedValue);
            var categoria = (from f in db.Categories
                            where f.CategoryID == id
                            select f).SingleOrDefault();

            txtNombre.Text = categoria.CategoryName;
            txtDescripcion.Text = categoria.Description;
        }
    }
}