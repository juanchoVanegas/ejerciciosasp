﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="categorias.aspx.cs" Inherits="Ejemplo2ASP.categorias" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

    <h1>Gestión de Categorías</h1>
    <form id="form1" runat="server">
    <div id="divMaestro">

        <div>
            <label for="txtFiltro">Filtrar:</label>
            <asp:TextBox runat="server" ID="txtFiltro" />
            <asp:Button Text="B" ID="btnFiltrar" runat="server" OnClick="btnFiltrar_Click" />
        </div>
        <div>
            <asp:ListBox runat="server" ID="lstCategorias" OnSelectedIndexChanged="lstCategorias_SelectedIndexChanged" AutoPostBack="true">
            </asp:ListBox>
        </div>
    </div>
    <div id="divDetalles">

        <fieldset>
            <legend>Detalles</legend>  
            
            <div>
                <label for="txtNombre">Nombre Categoría:</label>
                <asp:TextBox runat="server" ID="txtNombre" />
            </div>
            <div>
                <label for="txtDescripcion">Descripción Categoría:</label>
                <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" />
            </div>

            <div>
                <asp:Button Text="Eliminar" runat="server" ID="btnEliminar" />
                <asp:Button Text="Actualizar" runat="server" ID="btnActualizar" />
                <asp:Button Text="Agregar" runat="server" ID="btnAgregar" />
                <asp:Button Text="Limpiar" runat="server" ID="btnLimpiar" />
            </div>

        </fieldset>   


    </div>

    
    

    </form>
</body>
</html>
